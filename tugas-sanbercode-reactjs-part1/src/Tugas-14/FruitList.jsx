import React,{useContext,useState} from 'react'
import {FruitContext} from './ContextFruit'
import axios from 'axios'

const FruitList=()=>{
  const [daftarbuah,setDaftarBuah,input,setInput] =useContext(FruitContext);

    const editForm = (event) =>{
        let idBuah= parseInt(event.target.value)
        let buah = daftarbuah.find(x=> x.id === idBuah)
        setInput({...input,id: buah.id, name: buah.name , price:buah.price ,weight:buah.weight})    
      }
    const deleteForm = (event) =>{
        let idBuah= parseInt(event.target.value) 
        axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${idBuah}`)
        .then(() => {
          var newDataBuah = daftarbuah.filter(x=> x.id !== idBuah)
          setDaftarBuah(newDataBuah)
        })
      }

    return(
        <div className="App">
        <div className="container">
            <div className="pembelianBuah1">   
               <h1> Tabel Harga Buah</h1>
          <h1>Daftar Harga Buah</h1>
          <table className="tabel10">
            <thead>
              <tr className="thead">
                <th>No</th>
                <th>Nama</th>
                <th>Harga</th>
                <th>Berat</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody className="tbody">
                {
                  daftarbuah !== null &&daftarbuah.map((z, index)=>{
                    return(                    
                      <tr key={z.id}>
                        <td>{index+1}</td>
                        <td>{z.name}</td>
                        <td>{z.price}</td>
                        <td>{z.weight} kg</td>
                        <td>
                          <button  value={z.id} onClick={editForm}>Edit</button>
                          &nbsp;
                          <button  value={z.id} onClick={deleteForm}>Delete</button>
                        </td>
                      </tr>
                    )
                  })
                }
            </tbody>
          </table>
          </div>
          </div>
          </div>
           
    )
}

export default FruitList
