import React from 'react';
import FruitProvide from './ContextFruit'
import FruitList from './FruitList'
import FruitForm from './FruitForm'

const Tugas14=()=>{
    return(
        <FruitProvide>
            <FruitList/>
            <FruitForm/>
        </FruitProvide>
    )
}

export default Tugas14