import React, { useState,createContext } from 'react'

export const FruitContext =createContext();
 const FruitProvide = props=>{
    const [daftarbuah,setDaftarBuah]=useState(null)
    const [input,setInput]=useState({id:null,name:"",price:"" ,weight:parseInt("")})

    return(
        <FruitContext.Provider value={[daftarbuah,setDaftarBuah,input,setInput]}>
            {props.children}
        </FruitContext.Provider>
    )
}
export default FruitProvide