import React,{useContext,useEffect} from 'react'
import {FruitContext} from './ContextFruit'
import axios from 'axios'

const FruitForm=()=>{
    const [daftarbuah,setDaftarBuah,input,setInput] =useContext(FruitContext);

    useEffect(()=>{
        if(daftarbuah === null){
            axios.get('http://backendexample.sanbercloud.com/api/fruits')
            .then(res=>{
                setDaftarBuah(res.data)
                console.log(res.data)
            })
        }
    },[daftarbuah]);

    const submitForm = (event) =>{
        event.preventDefault()
        if ( input.id === null){
          axios.post(`http://backendexample.sanbercloud.com/api/fruits`,input)
          .then(res => {
            setDaftarBuah([...daftarbuah,input])
           setInput({id:"", name:"" , price:"" ,weight:""})
            
          
          })
    
        }else{
    
          axios.put(` http://backendexample.sanbercloud.com/api/fruits/${input.id}`, input)
          .then(res => {
            var newDaftarBuah = daftarbuah.map(x => {
              if (x.id === input.id){
                x.name = input.name
                x.price = input.price
                x.weight = input.weight
              }
              return x
            })
            setDaftarBuah(newDaftarBuah)
            setInput({id:null, name: "" , price:"" ,weight:parseInt("")})
          })
        }
      }


    const changeInputName = (event) =>{
        let temp=input
        let value= event.target.value
        switch(event.target.name){
            case 'nama' :
                temp.name=value
                setInput(
                    {...temp}
                )
                break
                case 'berat' :
                    temp.weight=value
                    setInput(
                        {...temp}
                    )
                    break
                    case 'harga' :
                        temp.price=value
                        setInput(
                            {...temp}
                        )
                        break

            default:break
        }
      }

  

    return(
        <>
        <h1>Form Daftar Harga Buah</h1>
              <div style={{width: "50%", margin: "0 auto", display: "block"}}>
                <div style={{border: "1px solid #aaa", padding: "20px"}}>
                  <form onSubmitCapture={submitForm} method="POST" action="apapu.html" >
                    <label style={{float: "left"}}>
                      Nama:
                    </label>
                    <input required style={{float: "right"}} type="text" name="nama" value={input.name} onChange={changeInputName}/>
                    <br/>
                    <br/>
                    <label style={{float: "left"}}>
                      Harga:
                    </label>
                    <input style={{float: "right"}} required type="text" name="harga" value={input.price} onChange={changeInputName}/>
                    <br/>
                    <br/>
                    <label style={{float: "left"}}>
                      Berat (dalam gram):
                    </label>
                    <input style={{float: "right"}} required  type="number" name="berat" value={input.weight} onChange={changeInputName}/>
                    <br/>
                    <br/>
                    <div style={{width: "100%", paddingBottom: "20px"}}>
                      <button style={{ float: "right"}} >submit</button>
                    </div>
                  </form>
                </div>
              </div>
            </>
    )
}

export default FruitForm