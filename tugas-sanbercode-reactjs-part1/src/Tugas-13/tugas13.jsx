import React, { useState, useEffect} from "react"
import axios from 'axios'
import '../App.css'



  
const Tugas13= ()=> {
        const [daftarbuah,setDaftarBuah]=useState(null)
        const [input,setInput]=useState({id:null,name:"",price:"" ,weight:parseInt("")})
    
        useEffect(()=>{
            if(daftarbuah === null){
                axios.get('http://backendexample.sanbercloud.com/api/fruits')
                .then(res=>{
                    setDaftarBuah(res.data)
                    console.log(res.data)
                })
            }
        },[daftarbuah]);

        const submitForm = (event) =>{
            event.preventDefault()
            if ( input.id === null){
              axios.post(`http://backendexample.sanbercloud.com/api/fruits`,input)
              .then(res => {
                let data =res.data
                setDaftarBuah([...daftarbuah,input])
               setInput({id:"", name:"" , price:"" ,weight:""})
                
              
              })
        
            }else{
        
              axios.put(` http://backendexample.sanbercloud.com/api/fruits/${input.id}`, input)
              .then(res => {
                var newDaftarBuah = daftarbuah.map(x => {
                  if (x.id === input.id){
                    x.name = input.name
                    x.price = input.price
                    x.weight = input.weight
                  }
                  return x
                })
                setDaftarBuah(newDaftarBuah)
                setInput({id:null, name: "" , price:"" ,weight:parseInt("")})
              })
            }
          }


        const deleteForm = (event) =>{
            let idBuah= parseInt(event.target.value) 
            axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${idBuah}`)
            .then(res => {
              var newDataBuah = daftarbuah.filter(x=> x.id !== idBuah)
              setDaftarBuah(newDataBuah)
            })
          }

        const changeInputName = (event) =>{
            let temp=input
            let value= event.target.value
            switch(event.target.name){
                case 'nama' :
                    temp.name=value
                    setInput(
                        {...temp}
                    )
                    break
                    case 'berat' :
                        temp.weight=value
                        setInput(
                            {...temp}
                        )
                        break
                        case 'harga' :
                            temp.price=value
                            setInput(
                                {...temp}
                            )
                            break

                default:break
            }
          }

        const editForm = (event) =>{
            let idBuah= parseInt(event.target.value)
            let buah = daftarbuah.find(x=> x.id === idBuah)
        
            setInput({id: buah.id, name: buah.name , price:buah.price ,weight:buah.weight})
        
          }
        return(
            <><div className="App">
            <div className="container">
                <div className="pembelianBuah1">   
                   <h1> Tabel Harga Buah</h1>
              <h1>Daftar Harga Buah</h1>
              <table className="tabel10">
                <thead>
                  <tr className="thead">
                    <th>No</th>
                    <th>Nama</th>
                    <th>Harga</th>
                    <th>Berat</th>
                    <th>Aksi</th>
                  </tr>
                </thead>
                <tbody className="tbody">
                    {
                      daftarbuah !== null &&daftarbuah.map((item, index)=>{
                        return(                    
                          <tr key={item.id}>
                            <td>{index+1}</td>
                            <td>{item.name}</td>
                            <td>{item.price}</td>
                            <td>{item.weight} kg</td>
                            <td>
                              <button  value={item.id} onClick={editForm}>Edit</button>
                              &nbsp;
                              <button  value={item.id} onClick={deleteForm}>Delete</button>
                            </td>
                          </tr>
                        )
                      })
                    }
                </tbody>
              </table>
              {/* Form */}
              <h1>Form Daftar Harga Buah</h1>
              <div style={{width: "50%", margin: "0 auto", display: "block"}}>
                <div style={{border: "1px solid #aaa", padding: "20px"}}>
                  <form onSubmitCapture={submitForm} method="POST" action="apapu.html" >
                    <label style={{float: "left"}}>
                      Nama:
                    </label>
                    <input required style={{float: "right"}} type="text" name="nama" value={input.name} onChange={changeInputName}/>
                    <br/>
                    <br/>
                    <label style={{float: "left"}}>
                      Harga:
                    </label>
                    <input style={{float: "right"}} required type="text" name="harga" value={input.price} onChange={changeInputName}/>
                    <br/>
                    <br/>
                    <label style={{float: "left"}}>
                      Berat (dalam gram):
                    </label>
                    <input style={{float: "right"}} required  type="number" name="berat" value={input.weight} onChange={changeInputName}/>
                    <br/>
                    <br/>
                    <div style={{width: "100%", paddingBottom: "20px"}}>
                      <button style={{ float: "right"}} >submit</button>
                    </div>
                  </form>
                </div>
              </div>
              </div>
              </div>
              </div>
            </>
           )
}


export default Tugas13
