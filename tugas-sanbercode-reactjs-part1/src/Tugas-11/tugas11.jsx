import React from 'react'

class Tugas11 extends React.Component{
    constructor(props){
        super(props);
        this.state={
            waktu:105,
            time:new Date(),
            dissapear:true
            
        }
        
    }
    perdetik(){
        this.setState({
            waktu:this.state.waktu-1
        })
    }
    realtime(){
        this.setState({
            time:new Date()
        })
    }
  
    componentDidMount(){
            if(this.props.start !==undefined){
                this.setState({waktu:this.props.start});
            }
            this.waktuID=setInterval(
                ()=>
                    this.perdetik(),1000
            );
            this.timeID= setInterval(
                ()=>this.realtime(),1000
            );

    }

    componentDidUpdate(){
        if(this.state.waktu===0 && this.state.dissapear===true){
            this.componentWillUnmount()
            this.setState({
                dissapear:false
            })
        }
    }
    componentWillUnmount(){
        clearInterval(this.waktuID);
        clearInterval(this.timeID);
       
    }
    render(){
        return(
            <>
            
            {this.state.dissapear && (
                <>
                <div className="App">
                <div className="container" >
                <div className="pembelianBuah1"> 
            <table>
                    <tr className="time">
                    <td><h1 >ini {this.state.time.toLocaleTimeString('en-US')}</h1> </td>
                        <td><h1 >hitung mundur : {this.state.waktu}</h1> </td>
                    </tr>
                </table>
                </div>
            </div>
            </div>
               </>
                )}
            </>
        )
    }
}
export default Tugas11;