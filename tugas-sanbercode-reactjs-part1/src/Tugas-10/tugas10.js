import React from 'react';


let dataHargaBuah = [
    {nama: "Semangka", harga: 10000, berat: 1000},
    {nama: "Anggur", harga: 40000, berat: 500},
    {nama: "Strawberry", harga: 30000, berat: 400},
    {nama: "Jeruk", harga: 30000, berat: 1000},
    {nama: "Mangga", harga: 30000, berat: 500}
  ]

  class Testing extends React.Component{
      render(){
          return(
              <>
                <tr className="tbody">
                                    <td>{this.props.nama}</td>
                                    <td>{this.props.harga}</td>
                                    <td>{this.props.berat/1000} kg</td>
                                </tr>
              </>
          )
      }
  }
class Mapping extends React.Component{
    render(){
        return(
            <>
                {
                    dataHargaBuah.map(tarik=>{
                        return(
                            <>
                                <Testing nama={tarik.nama} harga={tarik.harga} berat={tarik.berat}/>
                            </>
                        )
                    })
                }
            </>
        )
    }
}
class Tugas10 extends React.Component{
    render(){
        return(<>
              <div className="App">
            <div className="container">
                <div className="pembelianBuah1">   
                   <h1> Tabel Harga Buah</h1>
                        <table className="tabel10">
                            <thead>
                                <tr className="thead">
                                    <th>Nama</th>
                                    <th>Harga</th>
                                    <th>Berat</th>
                                </tr>
                            </thead>
                            <tbody>
                            <Mapping/>
                            </tbody>
                                    
                        </table>
                   
                </div>
            </div>
          </div>
            </>
        )
    }
}
export default Tugas10
