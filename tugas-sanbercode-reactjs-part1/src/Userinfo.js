import React from 'react';

/*
function App() {
  return (
    <div className="App">
      <div className="container">
          <div className="pembelianBuah">
              <h1>Form Pembelian Buah</h1>
              <form action="#">
                  <table>
                    <tr>
                      <th>Nama Pelanggan</th>
                      <td><input className="text" type="text"></input></td>
                    </tr>
                    <tr>
                      <th>Nama Pelanggan</th>
                      <td>
                          <input type="checkbox" ></input>
                          <label> Semangka</label><br></br>
                          <input type="checkbox" ></input>
                          <label> Jeruk</label><br></br>
                          <input type="checkbox" ></input>
                          <label> Nanas</label><br></br>
                          <input type="checkbox" ></input>
                          <label> Anggur</label>
                      </td>
                    </tr>
                    <tr>
                      <button type="submit">Kirim</button>
                    </tr>
                  </table>
              </form>
          </div>
      </div>
    </div>
  );
}
*/

class Welcome extends React.Component {
  render() {
    return <h1>Hello, {this.props.name}</h1>;
  }
}

class ShowAge extends React.Component {
  render() {
    return <h1>your age is {this.props.age}</h1>;
  }
}

var person = [
  {name: "sarah", age: 25},
  {name: "michael", age: 30},
  {name: "john", age: 33}
]

class UserInfo extends React.Component {
  render() {
    return (
      <>
        {person.map(el=> {
          return (
            <div style={{border: "1px solid #000", padding: "20px"}}>
              <Welcome name={el.name}/> 
              <ShowAge age={el.age}/> 
            </div>
          )
        })}
      </>
    )
  }
}

export default UserInfo;
