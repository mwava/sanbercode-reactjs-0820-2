import React from 'react'
import {Link} from 'react-router-dom'
import './tugas15.css'

 const Navigation = ()=>{
    return(
        <div className="colored">
            <nav className="listTugas">
                <ul>
                    <li><Link to=""><a>Home</a></Link></li>
                    <li><Link to="/Tugas-9"><a>Tugas9 Pengenalan React</a></Link></li>
                    <li><Link to="/Tugas-10"><a>Tugas10 Component & Props</a></Link></li>
                    <li><Link to="/Tugas-11"><a>Tugas11 State & Components Life cycle</a></Link></li>
                    <li><Link to="/Tugas-12"><a>Tugas12 List & Form</a></Link></li>
                    <li><Link to="/Tugas-13"><a>Tugas13 Hook & Rest API dengan axios</a></Link></li>
                    <li><Link to="/Tugas-14"><a>Tugas14 Context</a></Link></li>
                </ul>
            </nav>
        </div>
    )
}

export default Navigation