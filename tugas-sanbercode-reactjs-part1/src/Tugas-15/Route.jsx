import React from 'react'
import {BrowserRouter as Router} from 'react-router-dom'
import  Switcher  from './Switcher'
import Navigation from './Navigation'
import ThemeProvide from './ContextTheme'


 const Tugas15 = ()=>{
    return(
        <Router>
            <Navigation/>
            <Switcher/>
        </Router>
       
    )
}

export default Tugas15