import React,{useContext} from 'react'
import ThemeProvide, {ThemeContext} from './ContextTheme'


const ThemeChanges=()=>{
    
    const [theme,setTheme]=useContext(ThemeContext)
    
    const toogle=()=>{
        if(theme !== null){
            setTheme({dark:{color:"white",background:"black"}})
        }else setTheme({light:{color:"black",background:"white"}})
    }

    return(
        <>
        <ThemeProvide value={setTheme.dark}>
            <button onClick={toogle}></button>
        </ThemeProvide>
        </>
    )
}

export default ThemeChanges