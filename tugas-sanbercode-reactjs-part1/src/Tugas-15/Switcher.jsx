import React from 'react'
import Tugas9 from '../Tugas-9/tugas9'
import Tugas10 from '../Tugas-10/tugas10'
import Tugas11 from '../Tugas-11/tugas11'
import Tugas12 from '../Tugas-12/tugas12'
import Tugas13 from '../Tugas-13/tugas13'
import Tugas14 from '../Tugas-14/Fruit'

import { Switch,Route} from 'react-router-dom'

 const Switcher=()=>{
    return(
    <Switch>
        <Route exact path='/'><Tugas9/></Route>
        <Route path='/Tugas-9'><Tugas9/></Route>
        <Route path='/Tugas-10'><Tugas10/></Route>
        <Route path='/Tugas-11'><Tugas11/></Route>
        <Route path='/Tugas-12'><Tugas12/></Route>
        <Route path='/Tugas-13'><Tugas13/></Route>
        <Route path='/Tugas-14'><Tugas14/></Route>
    </Switch>

    )
}

export default Switcher