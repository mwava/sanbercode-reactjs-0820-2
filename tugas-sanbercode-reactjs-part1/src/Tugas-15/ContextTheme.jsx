import React, { useState,createContext } from 'react'

export const ThemeContext =createContext();
 const ThemeProvide = props=>{
    const [theme,setTheme]=useState({
        dark:{
            color: "white",
            backgroud: "black"
        },
        light:{
            color: "black",
            background: "white"
        }
    })
    

    return(
        <ThemeContext.Provider value={[theme,setTheme]}>
            {props.children}
        </ThemeContext.Provider>
    )
}
export default ThemeProvide